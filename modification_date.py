#!/usr/bin/python
#--------------------------------------------------------------------------------------------
# Version  1
#Author: Gavrilov Andrew
#Email: andrey.gavrilov@x5.ru
#--------------------------------------------------------------------------------------------
import os
from datetime import datetime
import time


def modification_date(filename):
    t = os.path.getmtime(filename)
    t = str(datetime.fromtimestamp(t))
    now = time.time()
    now = str(datetime.fromtimestamp(now))
    if t[8:10] == now[8:10]:
        return 1
    else:
        return 0


if __name__ == "__main__":
    print modification_date('/usr/local/gkretail/bo/data/standard_stamm.gdb.zip')
